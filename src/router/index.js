
import { createRouter, createWebHashHistory } from 'vue-router'
import newestPage from '../pages/newestPage'
import pastPage from '../pages/pastPage'
import commentsPage from '../pages/commentsPage'
import askPage from '../pages/askPage'
import showPage from '../pages/showPage'
import jobsPage from '../pages/jobsPage'
import submitPage from '../pages/submitPage'
import YPage from '../pages/YPage.vue'
import loginPage from '../pages/loginPage.vue'
import mainPage from '../pages/mainPage.vue'
import show_new_page from '../pages/additional_pages/show_new_page.vue'
import rules_page from '../pages/additional_pages/rules_page.vue'
import rules_faq from '../pages/additional_pages/rules_faq.vue'
import forgot_page from '../pages/additional_pages/forgot_page.vue'
import newsguidelines from '../pages/additional_pages/newsguidelines.vue'
import lists_page from '../pages/additional_pages/lists_page.vue'
import security_page from '../pages/additional_pages/security_page.vue'
import _id from '../pages/comments/_id.vue'
export default createRouter({
    history: createWebHashHistory(),
    routes:
        [
            {
                path: '/',
                component: YPage
            },
            {
                path: '/news',
                component: mainPage
            },
            {
                path: '/newest',
                component: newestPage,
            },
            {
                path: '/front',
                component: pastPage
            },
            {
                path: '/newcomments',
                component: commentsPage
            },
            {
                path: '/ask',
                component: askPage
            },
            {
                path: '/show',
                component: showPage,
            },
            {
                path: '/shownew',
                component: show_new_page
            },
            {
                path: '/showhn',
                component: rules_page
            },
            {
                path: '/newsfaq',
                component: rules_faq
            },
            {
                path: '/jobs',
                component: jobsPage
            },
            {
                path: '/submit',
                component: submitPage
            },
            {
                path: '/login',
                component: loginPage
            },
            {
                path: '/forgot',
                component: forgot_page
            },
            {
                path: '/newsguidelines',
                component: newsguidelines
            },
            {
                path: '/lists',
                component: lists_page
            },
            {
                path: '/security',
                component: security_page
            },
            {
                path:'/comments/:id',
                component: _id
            },
           
        ]
})
