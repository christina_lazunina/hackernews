import { createApp } from 'vue'
import HackerNews from './HackerNews'
import CreateRouter from './router'

createApp(HackerNews).use(CreateRouter).mount('#app')
